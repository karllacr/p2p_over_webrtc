<h3> Chat com P2P/WebRTC </h3>
Este repositório é dedicado a mostrar um simples tutorial de como criar um
chat utilizando alguns conceitos de P2P e uma das funcionalidades do WebRTC.<br/>

A primeira coisa a se fazer é garantir que <i>npm</i> esteja corretamente
instalados no seu computador. Esse gerenciador irá permitir que as instalações
dos pacotes necessários sejam feitas de forma rápida e correta.
Para instalá-lo basta, em seu terminal linux, usar o seguinte comando:
```
curl http://npmjs.org/install.sh | sudo sh

```

<h3> Pequena introdução ao P2P e WebRTC </h3>
O P2P (peer to peer) é uma arquitetura de rede de computadores que preza pela 
descentralização das funções, ou seja, não necessita de um computador servidor,
ou de um computador cliente. Dentro desse contexto o <b>WebRTC</b> é um projeto
opensource que possibilita troca de mensagens de texto, áudio ou voz usando 
streaming via P2P.

<h3> E agora, como fazer funcionar? </h3>
A primeira coisa a se fazer é clonar este diretório em seu computador e abrir
um terminal a partir dele. Todos os comandos a partir de agora devem partir de
dentro deste diretório.

Depois de instalar o <i>npm</i> vamos usá-lo para instalar um pacote chamado
<i>simple-peer</i>. É uma abstração do WebRTC que nos permiti codificar de forma
mais simples e rápida. Para instalar
basta: 

```
npm install simple-peer --save

```

Após isso, dê o comando:

```
npm install
```

para que todos as  dependencias do arquivo <b>package.json</b> sejam instaladas. 

No arquivo <i>index.js</i> nós instaciamos os peers através de:

```
var Peer = require('simple-peer')
  var peer = new Peer()
```

No arquivo <i>index.html</i> foram criados quadros que permitem a troca de 
mensagens entre os dois peers que iremos criar. Dentro deles iremos colocar os
ID's gerados pelos eventos criados no arquivo javascript.

<h3> Iniciando o serviço </h3>
No terminal iremos digitar o seguinte comando para iniciarmos o serviço:

```
npm start

```
Isso irá iniciar o serviço e permitir que a troca de mensagem seja feita.
Para abrir é preciso que você inicie um navegador de internet e na barra de 
URL digite:

```
localhost:9966/#init
```

O termo <i>#init</i> nos permite diferenciar quem é o peer que inicia a 
comunicação. Abra outra aba no navegador e digite agora, sem o <i>#init</i>:

```
localhost:9966
```
Essa segunda aba será o segundo peer que conversa com o primeiro.
Dentro do conceito do WebRTC temo um termo chamado <b>Session Decription
Protocol</b> que serve como uma espécie de chave para que peers estabeleçam
conexões entre si.

<h3> Obtendo o SDP </h3>
Na primeira aba que abrimos, depois de um certo tempo, na caixa de <b> Your ID</b>
será disponinilizado um SDP para que seja estabelecida a conexão com o segundo
peer. Copie esse texto e, na outra aba, cole no quadro <b>Other ID</b>.

Na segunda aba, depois de colar o SDP clique em <b>connect</b> de depois de 
algum tempo outro SDP será gerado. Copie-o e cole-o no campo <b>Other ID</b>
da primeira aba.

![](https://gitlab.com/karllacr/p2p_over_webrtc/raw/master/foto.jpg)
![](https://gitlab.com/karllacr/p2p_over_webrtc/raw/master/foto(1).jpg)

Sim, sim! É meio confuso, mas funciona. Depois e colar e copiar e colar e copiar,
a conexão estará estabelecida e a troca de mensagem entre as duas abas será 
possível.

É um exemplo simples, mas que nos permite visualizar o funcionamento da 
arquitetura peer to peer que pode ser expandida para vários âmbitos.

<h3>Referências</h3>
https://github.com/shama/letswritecode/tree/master/p2p-video-chat-webrtc